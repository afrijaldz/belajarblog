<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests;

class BlogController extends Controller
{
    public function index()
    {
      $post = Post::paginate(10);
      return view('blog.index')->with('posts', $post);
    }
    public function single($slug)
    {
      // fetch on the DB based on slug
      $post = Post::where('slug', '=', $slug)->first();

      // return the view and pass in the post oci_fetch_object
      return view('blog.single')->with('posts', $post);
    }
}
