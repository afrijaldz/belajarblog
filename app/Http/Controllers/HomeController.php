<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests;
use DB;

class HomeController extends Controller
{
  public function home()
  {
    $posts = Post::OrderBy('created_at', 'desc')->paginate(10);
    return view('page.home')->with('posts', $posts);
  }

    public function about()
    {
      return view('page.about');
    }

    public function contact()
    {
      return view('page.contact');
    }
}
