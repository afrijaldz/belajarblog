<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // create a variable and store all the blog posts in it from the database
        $posts = Post::OrderBy('created_at', 'desc')->paginate(10);

        // return a view and pass in the above variable
        return view('posts.index')->with('posts', $posts);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
          'title' =>  'required|max:225',
          'slug'  =>  'required|alpha_dash|min:5|max:255',
          'body'  =>  'required'
        ]);
        // store in the database
        $post = new Post;

        $post->title = $request->title;
        $post->slug  = $request->slug;
        $post->body = $request->body;
        $post->save();

        Session::flash('success', 'the post was successfully posted');

        // redirect to another page
        return redirect()->route('posts.show', $post->id);
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate the data
        $post = Post::find($id);

        if ($request->input('slug') == $post->slug) {
          $this->validate($request, [
            'title' =>  'required|max:255',
            'body'  =>  'required'
          ]);
        }
        else {
          $this->validate($request, [
            'title' =>  'required|max:255',
            'slug'  =>  'required|alpha_dash|min:5|max:255|unique:posts,slug',
            'body'  =>  'required'
          ]);
        }
        
        // Save the data to the database
        $post = Post::find($id);

        $post->title = $request->input('title');
        $post->slug  = $request->input('slug');
        $post->body = $request->input('body');
        $post->save();

        Session::flash('success', 'the post was successfully updated');

        // redirect to another page
        return redirect()->route('posts.show', $post->id);
        // redirect view with flash message to posts.show
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        $post->delete();

        Session::flash('success', 'The pos was successfully deleted');

        return redirect()->route('posts.index');
    }

}
