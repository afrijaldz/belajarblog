<?php

Route::group(['middleware' => ['web']], function(){
  // Authentication Routes
  Route::get('auth/login', 'Auth\AuthController@getlogin');
  Route::post('auth/login', 'Auth\AuthController@postlogin');
  Route::get('auth/logout', 'Auth\AuthController@getogout');

  // Registration Routes
  Route::get('auth/register', 'Auth\AuthController@getregister');
  Route::post('auth/register', 'Auth\AuthController@postregister');

  Route::get('blog/{slug}', ['as' => 'blog.single', 'uses' => 'BlogController@single'])->where('slug', '[w\d\-\_]+');
  Route::get('blog', ['uses' => 'BlogController@index', 'as' => 'blog.index']);
  Route::get('/',       ['uses' => 'HomeController@home',     'as' => 'home' ]);
  Route::get('about',   ['uses' => 'HomeController@about',    'as' => 'about' ]);
  Route::get('contact', ['uses' => 'HomeController@contact',  'as' => 'contact' ]);
  Route::resource('posts', 'PostController');

});
