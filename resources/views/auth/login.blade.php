@extends('layout.master')
@section('title')
  Login
@endsection
@section('konten')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      {!! Form::open() !!}

      {{ Form::label('email', 'Email :') }}
      {{ Form::email('email', null, ['class' => 'form-control']) }}
      {{ Form::label('password', 'Password :') }}
      {{ Form::password('password', ['class' => 'form-control']) }}
      {{ Form::label('remember', 'Keep logged in') }} {{ Form::checkbox('remember') }}
      <br>
      {{ Form::submit('Login', ['class' => 'btn btn-success btn-block form-spacing-top']) }}

      {!! Form::close() !!}
    </div>
  </div>
@endsection
