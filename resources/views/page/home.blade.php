@extends('layout.master')
@section('header-about')
  <li><a href="{{ route('about') }}">About</a></li>
@endsection
@section('header-contact')
  <li><a href="{{ route('contact') }}">Contact</a></li>
@endsection
@section('title')
  Beranda
@endsection
@section('konten')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <h1>Welcome to this blog</h1>
          <p class="lead">Thank you for visiting</p>
          <p><a class="btn btn-primary btn-lg" href="#" role="button">Popular Post</a></p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="post">
          @foreach($posts as $post)
          <h3>{{ $post->title }}</h3>
          <p>
            {{ substr($post->body, 0, 50) }} {{ strlen($post->body) > 50 ? "..." : "" }}
          </p>
          <a href="{{ route('posts.show', $post->id) }}" class="btn btn-primary">Read More</a>
        @endforeach
        </div>
      </div>
      <div class="col-md-3 col-md-offset-1">
          <h2>Sidebar</h2>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="text-center">
        {!! $posts->links(); !!}
      </div>
    </div>
  </div>
@endsection
