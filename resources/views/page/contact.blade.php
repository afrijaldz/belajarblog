@extends('layout.master')
@section('header-about')
  <li><a href="{{ route('about') }}">About</a></li>
@endsection
@section('header-contact')
  <li class="active"><a href="{{ route('contact') }}">Contact</a></li>
@endsection
@section('title')
About
@endsection
@section('konten')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Contact Me</h1>
        <hr>
        <form class="" action="index.html" method="post">
          <div class="form-group">
            <label name="email">Email :</label>
            <input type="text" name="email" id="email" class="form-control">
          </div>
          <div class="form-group">
            <label name="email">Subject :</label>
            <input type="text" name="subject" id="subject" class="form-control">
          </div>
          <div class="form-group">
            <label name="email">Message :</label>
            <textarea name="message" class="form-control" placeholder="Type your message here...."></textarea>
          </div>
          <input type="submit" name="submit" value="Send Message" class="btn btn-success">
        </form>
      </div>
    </div>
  </div>
@endsection
