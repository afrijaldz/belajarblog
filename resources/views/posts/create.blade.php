@extends('layout.master')
@section('title')
Post Something
@endsection
@section('konten')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h1>Create New Post</h1>
        <hr>
        {!! Form::open(['route' => 'posts.store']) !!}
          {{ Form::label('title', 'Title :') }}
          {{ Form::text('title', null, ['class' => 'form-control']) }}

          {{ Form::label('slug', 'Slug :') }}
          {{ Form::text('slug', null, ['class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255']) }}

          {{ Form::label('body', 'Body :', ['style' => 'margin-top: 10px']) }}
          {{ Form::textarea('body', null, ['class' => 'form-control']) }}

          {{ Form::submit('Publish', ['class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px']) }}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection
