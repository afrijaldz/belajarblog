@extends('layout.master')
@section('title')
  Main Dashboard
@endsection
@section('konten')
  <div class="row">
    <div class="col-md-10">
      <h1>All Post</h1>
    </div>
    <div class="col-md-2">
      {!! Html::linkRoute('posts.create', 'Create New Post', [null], ['class' => 'btn btn-lg btn-block btn-warning btn-h1-spacing']) !!}
    </div>
    <div class="col-md-12">
      <hr>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-condensed">
        <thead>
          <th> # </th>
          <th>title</th>
          <th>Body</th>
          <th>Created At</th>
          <th></th>
        </thead>
        <tbody>
          @foreach($posts as $post)
            <tr>
              <td>{{ $post->id }}</td>
              <td>{{ $post->title }}</td>
              <td>{{ substr($post->body, 0, 50) }} {{ strlen($post->body) > 50 ? "..." : "" }}</td>
              <td>{{ date('M j, Y', strtotime($post->created_at)) }}</td>
              <td>
                <a href="{{ route('posts.destroy', $post->id) }}"  class="btn-primary btn"><i class="glyphicon glyphicon-eye-open"></i></a>
                <a href="{{ route('posts.edit', $post->id) }}" class="btn-info btn"><i class="glyphicon glyphicon-pencil"></i></a>
              </td>
            </tr>


            <tr></tr>
          @endforeach
        </tbody>
      </table>
      <div class="text-center">
        {!! $posts->links(); !!}
      </div>
    </div>
  </div>
@endsection
